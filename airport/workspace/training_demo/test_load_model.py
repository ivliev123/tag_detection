import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile
import pickle

from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten
from tensorflow.keras.layers import Conv2D, MaxPooling2D

from collections import defaultdict
from io import StringIO
# from matplotlib import pyplot as plt
from PIL import Image


import cv2
cap = cv2.VideoCapture(1)

# This is needed since the notebook is stored in the object_detection folder.
# sys.path.append("..")
sys.path.append("/home/ivliev/TensorFlow/models/research/object_detection/")

from utils import label_map_util

from utils import visualization_utils as vis_util

sys.path.append("..")

# global graph,model
# graph = tf.get_default_graph()


with open('/media/ivliev/F045-E632/signs_img/label_class.pickle', 'rb') as f:
    label_class = pickle.load(f)


# global cnn_graph
# cnn_graph = tf.get_default_graph()
# new_model = tf.keras.models.load_model('/media/ivliev/F045-E632/signs_img/test15.model')

# tf_config = some_custom_config

# graph = tf.get_default_graph()
# sess = tf.Session(graph=graph)
# tf.compat.v1.keras.backend.set_session(sess)
# with graph.as_default():



model = tf.keras.models.load_model('/media/ivliev/F045-E632/signs_img/test15.model')
graph = tf.get_default_graph()
# model._make_predict_function()
# new_model._make_predict_function()

def recognition(img):

    # img = cv2.imread(img_path)
    img = cv2.resize(img,(int(50),int(50)))

    img = img/255.0
    data = img
    test_array = []
    test_array.append(data)
    test_array = np.array(test_array)

    # global cnn_graph
    # with cnn_graph.as_default():
    # new_model = tf.keras.models.load_model('/media/ivliev/F045-E632/signs_img/test15.model')
    # with graph.as_default(), tf.Session() as sess:
    #     predictions = model.predict(test_array)


    global graph
    with graph.as_default():
        predictions = model.predict(test_array)

    # print(predictions)
    return np.argmax(predictions[0])
    # return predictions

def cut_sign(image_np, boxes, scores):
    boxes = np.squeeze(boxes)
    scores = np.squeeze(scores)
    img = image_np
    h, w, d_  = img.shape
    for i in range(len(scores)):
        score = scores[i]
        if(score>0.7):
            x1 = int(boxes[i][1] * w)
            y1 = int(boxes[i][0] * h)
            x2 = int(boxes[i][3] * w)
            y2 = int(boxes[i][2] * h)

            x1 = int(x1)
            y1 = int(y1)
            x2 = int(x2)
            y2 = int(y2)

            sign = img[y1:y2, x1:x2]
            sign = np.array(sign)
            label = recognition(sign)

            # font = cv2.FONT_HERSHEY_SIMPLEX
            # org = (x1, y1)
            # fontScale = 1
            # color = (255, 0, 0)
            # thickness = 2
            # image = cv2.putText(image, str(label), org, font,fontScale, color, thickness, cv2.LINE_AA)


            img = cv2.rectangle(img, (x1,y1), (x2,y2), (0,0,255), 3)



# What model to download.
MODEL_NAME = '/media/ivliev/F045-E632/signs3/workspace/training_demo/trained-inference-graphs/output_inference_graph_v1.pb'
MODEL_FILE = MODEL_NAME + '.tar.gz'
DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/'
# Path to frozen detection graph. This is the actual model that is used for the object detection.
PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'
# List of the strings that is used to add correct label for each box.
PATH_TO_LABELS = os.path.join('annotations', 'label_map.pbtxt')
NUM_CLASSES = 1


# ## Load a (frozen) Tensorflow model into memory.
detection_graph = tf.Graph()
with detection_graph.as_default():
  od_graph_def = tf.GraphDef()
  with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
    serialized_graph = fid.read()
    od_graph_def.ParseFromString(serialized_graph)
    tf.import_graph_def(od_graph_def, name='')


# ## Loading label map
label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)


# ## Helper code
def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)





# In[10]:

with detection_graph.as_default():
  with tf.Session(graph=detection_graph) as sess:
    while True:
      ret, image_np = cap.read()
      # for i in range(300):
      # image_np = cv2.imread('/media/ivliev/F045-E632/signs3/workspace/training_demo/images/test/fone_new'+str(i)+'.jpg')
      # image_np = cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)
      # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
      image_np_expanded = np.expand_dims(image_np, axis=0)
      image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
      # Each box represents a part of the image where a particular object was detected.
      boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
      # Each score represent how level of confidence for each of the objects.
      # Score is shown on the result image, together with the class label.
      scores = detection_graph.get_tensor_by_name('detection_scores:0')
      classes = detection_graph.get_tensor_by_name('detection_classes:0')
      num_detections = detection_graph.get_tensor_by_name('num_detections:0')
      # Actual detection.
      (boxes, scores, classes, num_detections) = sess.run(
          [boxes, scores, classes, num_detections],
          feed_dict={image_tensor: image_np_expanded})

      # Visualization of the results of a detection.
      cut_sign(image_np, boxes, scores)

      vis_util.visualize_boxes_and_labels_on_image_array(
          image_np,
          np.squeeze(boxes),
          np.squeeze(classes).astype(np.int32),
          np.squeeze(scores),
          category_index,
          use_normalized_coordinates=True,
          line_thickness=3)

          # cv2.imwrite('/media/ivliev/F045-E632/signs3/workspace/training_demo/images/test2/image_np'+str(i)+'.jpg',image_np)

      cv2.imshow('object detection', cv2.resize(image_np, (800,600)))
      if cv2.waitKey(25) & 0xFF == ord('q'):
        cv2.destroyAllWindows()
        break
