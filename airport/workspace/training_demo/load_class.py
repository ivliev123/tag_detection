import tensorflow as tf
import numpy as np
import time
import cv2
import pickle
import sys

import socket
# import sys
# import cv2
# import pickle
import numpy as np
import struct ## new
import zlib


HOST=socket.gethostname()
PORT=8485

s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
print('Socket created')

s.bind((HOST,PORT))
print('Socket bind complete')
s.listen(10)
print('Socket now listening')

conn,addr=s.accept()

data = b""
payload_size = struct.calcsize(">L")
print("payload_size: {}".format(payload_size))



with open('/media/ivliev/F045-E632/signs_img/label_class.pickle', 'rb') as f:
    label_class = pickle.load(f)

label_class = np.array(label_class)
labels = label_class[:,0]
labels = list(labels)

with open('/media/ivliev/F045-E632/signs3/workspace/training_demo/class_img.pickle', 'rb') as f:
    class_img = pickle.load(f)

class_img = np.array(class_img)
classes = class_img[:,0]
classes = list(classes)

new_model = tf.keras.models.load_model('/media/ivliev/F045-E632/signs_img/test15.model')
new_model._make_predict_function()
print('model loaded') # just to keep track in your server


def recognition(img):

    # img = cv2.imread(img_path)
    img = cv2.resize(img,(int(50),int(50)))

    img = img/255.0
    data = img
    test_array = []
    test_array.append(data)
    test_array = np.array(test_array)

    # new_model = tf.keras.models.load_model('/media/ivliev/F045-E632/signs_img/test15.model')
    # print(new_model.summary())
    # x1 = time.time()
    # with tf.Session() as sess:
    predictions = new_model.predict(test_array)
    # x2 = time.time()

    print(np.argmax(predictions[0]))
    return np.argmax(predictions[0]), predictions[0][np.argmax(predictions[0])]
    # return predictions

def cut_sign(image_np, boxes, scores):
    boxes = np.squeeze(boxes)
    scores = np.squeeze(scores)
    img = image_np
    # print(img)
    # print(img.shape)
    h, w, d_  = img.shape
    for i in range(len(scores)):
        score = scores[i]
        if(score>0.7 and score != 1):
            x1 = int(boxes[i][1] * w)
            y1 = int(boxes[i][0] * h)
            x2 = int(boxes[i][3] * w)
            y2 = int(boxes[i][2] * h)

            x1 = int(x1)
            y1 = int(y1)
            x2 = int(x2)
            y2 = int(y2)

            sign = img[y1:y2, x1:x2]
            sign = np.array(sign)


            # img_counter += 1
            label, percent = recognition(sign)
            percent = percent*100
            percent = round(percent,2)

            if(percent>=85):
                print(label)
                index = labels.index(str(label))
                class_ = label_class[index][1]
                print(class_)
                try:
                    index_sign = classes.index(class_)
                    print(index_sign)
                    img_sign_to = class_img[index_sign][1]
                    hs, ws, d_s  = img_sign_to.shape

                    x_offset= x1 - ws
                    y_offset= y1 - hs
                    img[y_offset:y_offset+img_sign_to.shape[0], x_offset:x_offset+img_sign_to.shape[1]] = img_sign_to
                except ValueError:
                    print(class_)

                font = cv2.FONT_HERSHEY_SIMPLEX
                org = (x1, y1)
                fontScale = 1
                color = (255, 0, 0)
                thickness = 2
                str_out =  str(class_)+"  " + str(percent)+"%"
                img = cv2.putText(img, str(str_out), org, font,fontScale, color, thickness, cv2.LINE_AA)


                img = cv2.rectangle(img, (x1,y1), (x2,y2), (0,0,255), 3)

    # cv2.imwrite('/media/ivliev/F045-E632/image_np__'+str(i)+'.jpg',img)

def predictions_out(img_path, label_class):
    predictions = recognition(img_path)
    n = 3
    max_3_predict_index = np.argsort(predictions[0])[-n:]
    max_3_predict = predictions[0][max_3_predict_index]

    label_class = np.array(label_class)
    label = label_class[:,0]
    label = list(label)

    array_out = []
    for i in range(max_3_predict_index.size):
        index = label.index(str(max_3_predict_index[i]))
        class_ = label_class[index][1]
        if(max_3_predict[i] > 0.005):
            array_out.append([class_ , max_3_predict[i]*100.00])

    array_out = sorted(array_out, key=lambda x : x[1], reverse = True)

    return array_out


while True:
    while len(data) < payload_size:
        print("Recv: {}".format(len(data)))
        data += conn.recv(4096)

    print("Done Recv: {}".format(len(data)))
    packed_msg_size = data[:payload_size]
    data = data[payload_size:]
    msg_size = struct.unpack(">L", packed_msg_size)[0]
    print("msg_size: {}".format(msg_size))
    while len(data) < msg_size:
        data += conn.recv(4096)
    frame_data = data[:msg_size]
    data = data[msg_size:]

    frame=pickle.loads(frame_data, fix_imports=True, encoding="bytes")

    scores = frame[2]
    boxes = frame[1]
    frame = frame[0]
    # print(frame.shape)
    # cut_sign(frame, boxes, scores)

    frame = cv2.imdecode(frame, cv2.IMREAD_COLOR)
    cut_sign(frame, boxes, scores)
    # print(frame.shape)
    cv2.imshow('ImageWindow',frame)
    cv2.waitKey(1)



# with open('/media/ivliev/F045-E632/signs_img/test.pickle', 'rb') as f:
#     data = pickle.load(f)
#
# i = 0
# j = 0
#
#
# print(len(data))
# for i in range(len(data)):
#     img = data[i][0]
#     id_predict = recognition(img)
#
#     print()
#     print(id_predict)
#     print(data[i][1])
#
#     if(id_predict == data[i][1]):
#         i = i+1
#     else:
#         j = j+1
#
# print("+" + str(i))
# print("-" + str(j))
